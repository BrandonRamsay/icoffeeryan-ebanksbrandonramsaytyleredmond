# Members
1. Brandon Ramsay 1705815      
2. Ryan Ebanks 1700002
3. Tyler Edmond 1608052

# Project Description
Cups is a local coffee shop that provides a relaxing getaway in the middle of the city for the disabled community. 
They are also a wonderful example of a Social Enterprise Boost Initiative 
Kat, the manager, has been encouraged by her mentor to establish another store at 95 Moolean Avenue in the heart of Montego Bay. 
Kat would like to encourage an empowering environment through self service. Your consulting team providing pro bono services has considered to incorporate Artificial 
Intelligence through Computer Vision and Speech Processing to accomplish this. 
The touch-screen self service kiosk will allow customers to order their favourite treats and verify using their Digital Id.

# ERD
1. [ERD](https://drive.google.com/open?id=1vJqlwbs8uZQiUi38koniT5znoMitiRuv)
2. [WIREFRAME FOR CUPS](https://mail.google.com/mail/u/0/#inbox/FMfcgxwGDDnSGVxVjxwptdHDRRvMzfMS?projector=1&messagePartId=0.1)
